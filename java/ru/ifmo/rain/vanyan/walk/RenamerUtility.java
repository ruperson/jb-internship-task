package ru.ifmo.rain.vanyan.walk;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class RenamerUtility {
    public static void main(String[] args) {
        if (args == null || args.length != 1 || args[0] == null) {
            System.out.println("Invalid program arguments. Needed only one" +
                    "argument with a directory. ");
            return;
        }
        Path input;
        try {
            input = Paths.get(args[0]);
        } catch (InvalidPathException e) {
            System.out.println("Invalid program arguments. Invalid path name " + args[0]);
            return;
        }

        if (!input.toFile().isDirectory()) {
            System.out.println("Invalid program arguments." + args[0] + " is not a directory");
            return;
        }

        try (Stream<Path> walk = Files.walk(input)) {
            walk.filter(Files::isRegularFile)
                    .filter(path -> path.toString().endsWith(".java") || path.toString().endsWith(".kt"))
                    .forEach(RenamerUtility::rename);
        } catch (IOException e) {
            System.out.println("Error curred during walk" + e.getMessage());
        }
    }
    private static void rename(Path path) {
        String newName = path.toString() + ".2019";
        boolean renamed = path.toFile().renameTo(new File(newName));
        if (renamed) {
            System.out.println(newName);
        } else {
            System.out.println("Error while renaming " + path);
        }
    }

}